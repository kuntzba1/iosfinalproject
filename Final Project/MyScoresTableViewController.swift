//
//  MyScoresTableViewController.swift
//  Final Project
//
//  Created by kuntzba1 on 4/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit
import Parse
import Bolts

class MyScoresTableViewController: UITableViewController {
    let data = dataStorage(user: Singleton.sharedInstance.username)
   
    let fm = NSFileManager.defaultManager()
    
    var contents = ""
    var current = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(MyScoresTableViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        getFromFile()
    }
    
    func getFromFile(){
        NSOperationQueue.mainQueue().addOperationWithBlock({
        
        do{
            if self.fm.fileExistsAtPath(Singleton.sharedInstance.scorePath){
                self.contents = try String(contentsOfFile: Singleton.sharedInstance.scorePath,encoding: NSUTF8StringEncoding)
            }
            
            let scoreData = self.contents.componentsSeparatedByString("\n")
            self.data.scores = scoreData
        } catch{
            print("File not found")
        }
            
            self.tableView.reloadData()

        })
        
        
    }
    
    func refresh(sender:AnyObject)
    {
       
        self.data.scores.removeAll()
        getFromFile()
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.data.scores.removeAll()
        getFromFile()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func performLogout(){
        PFUser.logOut()
        performSegueWithIdentifier("unwindToLogin", sender: self)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Populate cells
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MyScoreCell
        cell.usernameLabel.text = Singleton.sharedInstance.username
        cell.scoreLabel.text = data.scores[indexPath.row]
        
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.scores.count
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        // Set currently selected row index
        current = indexPath.row
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Set section headers
        return "Usernames and Scores"
    }
}
