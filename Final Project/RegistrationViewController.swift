//
//  RegistrationViewController.swift
//  Auto-Mobile Dev
//
//  Created by kuntzba1 on 3/16/16.
//  Copyright © 2016 Auto-Mobile. All rights reserved.
//

import UIKit
import Parse
import Bolts


class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
    }
    
    @IBAction func dismissKeyboard() {
        // Dismisses keyboard
        self.view.endEditing(true)
    }
    
    @IBAction func doneButtonPress(sender: AnyObject) {
        dismissKeyboard()
        
        // Validate fields
        if usernameField.text?.characters.count < 8{
            // Display alertController for username character count
            let alert = UIAlertController(title: "Invalid", message: "Username must be at least 8 characters", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)

        }
        else if passwordField.text != confirmPasswordField.text{
            // Display alertController for match
            let alert = UIAlertController(title: "Invalid", message: "Passwords do not match", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if passwordField.text?.characters.count < 8{
            // Display alertController for password character count
            let alert = UIAlertController(title: "Invalid", message: "Password must be at least 8 characters", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if emailField.text?.containsString("@") == false{
            // Display alertController for incorrect email format
            let alert = UIAlertController(title: "Invalid", message: "Please enter a valid email address", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            // Run a spinner to show a task in progress
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            // Create new user object containing below
            let newUser = PFUser()
            
            newUser.username = usernameField.text
            newUser.password = passwordField.text
            newUser.email = emailField.text
            
            Singleton.sharedInstance.username = usernameField.text!

            // Sign up the user asynchronously
            newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
                
                // Stop the spinner
                spinner.stopAnimating()
                if ((error) != nil) {
                    // Display alertController for user creation error
                    let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                        (action) in
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
                else{
                    
                    // Display alertController for user creation success
                    let alert = UIAlertController(title: "Success", message: "You now have a Chocobo's Escape account under username '\(self.usernameField.text!)'", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                        (action) in
                        self.performDoneRegistration()
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    })
                }
            })
        
        }
    }
    
    func performDoneRegistration(){
        performSegueWithIdentifier("doneRegistration", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}