//
//  TabBarController.swift
//  Final Project
//
//  Created by kuntzba1 on 4/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}