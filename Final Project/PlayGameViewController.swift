//
//  ViewController.swift
//  Final Project
//
//  Created by kuntzba1 on 4/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit
import SpriteKit
import Parse
import Bolts
import AVFoundation

class PlayGameViewController: UIViewController {
    let fm = NSFileManager.defaultManager()
    
    var contents = ""
   
    
    var backgroundMusicPlayer = AVAudioPlayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playBackgroundMusic("08-cinco-de-chocobo.mp3")
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
        
        Singleton.sharedInstance.addObserver(self, forKeyPath: "gameOver", options: .New, context: &contextVar)
        Singleton.sharedInstance.addObserver(self, forKeyPath: "playing", options: .New, context: &contextVar)
        self.run()
    }
    
    func playBackgroundMusic(filename: String) {
        let url = NSBundle.mainBundle().URLForResource(filename, withExtension: nil)
        guard let newURL = url else {
            print("Could not find file: \(filename)")
            return
        }
        do {
            backgroundMusicPlayer = try AVAudioPlayer(contentsOfURL: newURL)
            backgroundMusicPlayer.numberOfLoops = -1
            backgroundMusicPlayer.prepareToPlay()
            backgroundMusicPlayer.play()
        } catch let error as NSError {
            print(error.description)
        }
    }

    
    func run(){
        if let scene = Scene.unarchiveFromFile("Scene") as? Scene {
            // Configure the view.
            let skView = self.view as! SKView
            
            // Sprite Kit applies additional optimizations to improve rendering performance
            skView.ignoresSiblingOrder = true
            
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .AspectFill
            skView.presentScene(scene)
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &contextVar && keyPath == "gameOver" {
            if let _ = change?[NSKeyValueChangeNewKey] {
                let alert = UIAlertController(title: "Game Over", message: "You scored \(Singleton.sharedInstance.score) points!", preferredStyle: UIAlertControllerStyle.Alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                    (action) in
                    var highScore = String()
                    var highScoreInt = Int()
                    if PFUser.currentUser()?.objectForKey("score") != nil{
                        highScore = PFUser.currentUser()?.objectForKey("score") as! String
                        highScoreInt = Int(highScore)!
                    }
                    else{
                        highScore = "0"
                        highScoreInt = Int(highScore)!
                    }
                    
                    if  highScoreInt < Singleton.sharedInstance.score{
                        PFUser.currentUser()?.setObject(String(Singleton.sharedInstance.score), forKey: "score")
                        PFUser.currentUser()?.saveInBackgroundWithBlock({ (success, error) -> Void in})
                    }
                    self.run()
                    self.navigationController?.navigationBarHidden = false
                }
                alert.addAction(okAction)
                self.presentViewController(alert, animated: true, completion: nil)
                NSOperationQueue.mainQueue().addOperationWithBlock({
                do{
                    if self.fm.fileExistsAtPath(Singleton.sharedInstance.scorePath){
                        self.contents = try String(contentsOfFile: Singleton.sharedInstance.scorePath,encoding: NSUTF8StringEncoding)
                    }
                    
                    var scoreData = self.contents.componentsSeparatedByString("\n")
                    scoreData.append(String(Singleton.sharedInstance.score))
                    let joined = scoreData.joinWithSeparator("\n")
                    try joined.writeToFile(Singleton.sharedInstance.scorePath, atomically: true, encoding: NSUTF8StringEncoding)
                } catch{
                    print("File not found")
                }
                })
            }
        }
        else if context == &contextVar && keyPath == "playing" {
            self.navigationController?.navigationBarHidden = true
        }
        else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    deinit{
        Singleton.sharedInstance.removeObserver(self, forKeyPath: "gameOver")
        Singleton.sharedInstance.removeObserver(self, forKeyPath: "playing")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
}
