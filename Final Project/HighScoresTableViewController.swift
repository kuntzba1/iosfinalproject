//
//  HighScoresTableViewController.swift
//  Final Project
//
//  Created by kuntzba1 on 4/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit
import Parse
import Bolts

class HighScoresTableViewController: UITableViewController {
    var usernameArray = [String]()
    var scoreArray = [String]()
    
    var current = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(HighScoresTableViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func pullFromParse(){
        
        PFUser.query()?.findObjectsInBackgroundWithBlock{
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                // For each object in the class object, append it to data array
                for object in objects! {
                    if object.objectForKey("score") != nil{
                        let data = object.objectForKey("username") as? NSString  as! String
                        self.usernameArray.append(data)
                        let data2 = object.objectForKey("score") as? NSString  as! String
                        self.scoreArray.append(data2)
                    }
                }
                self.tableView.reloadData()
            }
            else{
                print("error pulling high scores")
            }
        }
    }
    
    func refresh(sender:AnyObject)
    {
        usernameArray.removeAll()
        scoreArray.removeAll()
        pullFromParse()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewWillAppear(animated: Bool) {
        usernameArray.removeAll()
        scoreArray.removeAll()
        pullFromParse()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func performLogout(){
        PFUser.logOut()
        performSegueWithIdentifier("unwindToLogin", sender: self)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Populate cells
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! HighScoreCell
        cell.usernameLabel.text = usernameArray[indexPath.row]
        cell.scoreLabel.text = scoreArray[indexPath.row]
        
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreArray.count
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        // Set currently selected row index
        current = indexPath.row
        return indexPath
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        // Set section headers
        return "Usernames and Scores"
    }
    
    
}

