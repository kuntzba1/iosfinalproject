//
//  HighScoreCell.swift
//  Final Project
//
//  Created by kuntzba1 on 4/15/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit

class HighScoreCell: UITableViewCell{
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

