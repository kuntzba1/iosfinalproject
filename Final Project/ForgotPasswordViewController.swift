//
//  ForgotPasswordViewController.swift
//  Auto-Mobile Dev
//
//  Created by kuntzba1 on 3/17/16.
//  Copyright © 2016 Auto-Mobile. All rights reserved.
//

import UIKit
import Parse
import Bolts


class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
    }
    
    @IBAction func dismissKeyboard() {
        //Dismisses keyboard
        self.view.endEditing(true)        
    }
    
    @IBAction func doneButtonPress(sender: AnyObject) {
        dismissKeyboard()
        passwordReset()
    }
    
    func passwordReset() {
        let email = self.emailField.text
        let finalEmail = email!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        // Send a request to reset a password
        PFUser.requestPasswordResetForEmailInBackground(finalEmail) { (success, error) -> Void in
            if (error == nil) {
                //Display alert for successful email entry
                let success = UIAlertController(title: "Success", message: "Success! Check your email!", preferredStyle: .Alert)
                let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                success.addAction(okButton)
                self.presentViewController(success, animated: true, completion: nil)
                
            }else {
                //Display alert for invalid email entry
                let errormessage = error!.userInfo["error"] as! NSString
                let error = UIAlertController(title: "Invalid", message: errormessage as String, preferredStyle: .Alert)
                let okButton = UIAlertAction(title: "OK", style: .Default, handler: nil)
                error.addAction(okButton)
                self.presentViewController(error, animated: true, completion: nil)
                self.emailField.text = ""
            }
        }
    }
    
    @IBAction func unwindToForgotPassword (segue: UIStoryboardSegue){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}