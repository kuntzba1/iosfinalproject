//
//  Scene.swift
//  Final Project
//
//  Created by kuntzba1 on 4/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import SpriteKit

class Scene: SKScene, SKPhysicsContactDelegate{
    let verticalSwordGap = 170.0
    
    var avatar:SKSpriteNode!
    var wallColor:SKColor!
    var swordTextureUp:SKTexture!
    var swordTextureDown:SKTexture!
    var moveSwordsAndRemove:SKAction!
    var moving:SKNode!
    var swords:SKNode!
    var canRestart = Bool()
    var scoreLabelNode:SKLabelNode!
    var score = NSInteger()
    
    let avatarCategory: UInt32 = 1 << 0
    let worldCategory: UInt32 = 1 << 1
    let swordCategory: UInt32 = 1 << 2
    let scoreCategory: UInt32 = 1 << 3
    
    override func didMoveToView(view: SKView) {
        canRestart = true
        
        // setup physics
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -0.0)
        self.physicsWorld.contactDelegate = self
        
        // setup background color
        wallColor = SKColor.blackColor()
        self.backgroundColor = wallColor
        
        moving = SKNode()
        self.addChild(moving)
        swords = SKNode()
        moving.addChild(swords)
        
        // spikes
        let spikesTexture = SKTexture(imageNamed: "spikes")
        spikesTexture.filteringMode = .Nearest
        
        let moveSpikesSprite = SKAction.moveByX(-spikesTexture.size().width * 2.0, y: 0, duration: NSTimeInterval(0.02 * spikesTexture.size().width * 2.0))
        let resetSpikesSprite = SKAction.moveByX(spikesTexture.size().width * 2.0, y: 0, duration: 0.0)
        let moveSpikesSpritesForever = SKAction.repeatActionForever(SKAction.sequence([moveSpikesSprite, resetSpikesSprite]))
        
        var i:CGFloat = 0
        while i < 2.0 + self.frame.size.width / (spikesTexture.size().width * 2.0) {
            i++
            let sprite = SKSpriteNode(texture: spikesTexture)
            sprite.setScale(1.0)
            sprite.position = CGPoint(x: i * sprite.size.width, y: sprite.size.height/2)
            sprite.runAction(moveSpikesSpritesForever)
            moving.addChild(sprite)
        }
        
        // create the sword textures
        swordTextureUp = SKTexture(imageNamed: "SwordUp")
        swordTextureUp.filteringMode = .Nearest
        swordTextureDown = SKTexture(imageNamed: "SwordDown")
        swordTextureDown.filteringMode = .Nearest
        
        // create the sword movement actions
        let distanceToMove = CGFloat(self.frame.size.width + 2.0 * swordTextureUp.size().width)
        let moveSwords = SKAction.moveByX(-distanceToMove, y: 0.0, duration:NSTimeInterval(0.01 * distanceToMove))
        let removeSwords = SKAction.removeFromParent()
        moveSwordsAndRemove = SKAction.sequence([moveSwords, removeSwords])
        
        // spawn the swords
        let spawn = SKAction.runBlock({() in self.spawnSwords()})
        let delay = SKAction.waitForDuration(NSTimeInterval(2.0))
        let spawnThenDelay = SKAction.sequence([spawn, delay])
        let spawnThenDelayForever = SKAction.repeatActionForever(spawnThenDelay)
        self.runAction(spawnThenDelayForever)
        
        // setup the avatar (chocobo)
        let avatarTexture1 = SKTexture(imageNamed: "chocobo_no_wings_opt")
        avatarTexture1.filteringMode = .Nearest
        
        avatar = SKSpriteNode(texture: avatarTexture1)
        avatar.setScale(1.6)
        avatar.position = CGPoint(x: self.frame.size.width * 0.4, y:self.frame.size.height * 0.45)
        
        avatar.physicsBody = SKPhysicsBody(circleOfRadius: avatar.size.height / 2.0)
        avatar.physicsBody?.dynamic = true
        avatar.physicsBody?.allowsRotation = false
        avatar.physicsBody?.categoryBitMask = avatarCategory
        avatar.physicsBody?.collisionBitMask = worldCategory | swordCategory
        avatar.physicsBody?.contactTestBitMask = worldCategory | swordCategory
        
        self.addChild(avatar)
        
        // create the spikes
        let spikes = SKNode()
        spikes.position = CGPoint(x: 0, y: spikesTexture.size().height/2)
        spikes.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: self.frame.size.width, height: spikesTexture.size().height))
        spikes.physicsBody?.dynamic = false
        spikes.physicsBody?.categoryBitMask = worldCategory
        self.addChild(spikes)
        
        // Initialize label and create a label which holds the score
        score = 0
        scoreLabelNode = SKLabelNode(fontNamed:"Copperplate")
        scoreLabelNode.position = CGPoint(x: self.frame.midX, y: 4 * self.frame.size.height / 5)
        scoreLabelNode.zPosition = 100
        scoreLabelNode.text = String(score)
        self.addChild(scoreLabelNode)
        
        moving.speed = 0
        avatar.physicsBody?.collisionBitMask = worldCategory
        avatar.runAction(  SKAction.rotateByAngle(CGFloat(M_PI) * CGFloat(avatar.position.y) * 0.01, duration:1), completion:{self.avatar.speed = 0})
        
    }
    
    func spawnSwords() {
        let swordPair = SKNode()
        swordPair.position = CGPoint(x: self.frame.size.width + swordTextureUp.size().width * 2, y: 0)
        swordPair.zPosition = -10
        
        let height = UInt32( self.frame.size.height / 4)
        let y = Double(arc4random_uniform(height) + height);
        
        let swordDown = SKSpriteNode(texture: swordTextureDown)
        swordDown.setScale(2.0)
        swordDown.position = CGPoint(x: 0.0, y: y + Double(swordDown.size.height) + verticalSwordGap)
        swordDown.physicsBody = SKPhysicsBody(rectangleOfSize: swordDown.size)
        swordDown.physicsBody?.dynamic = false
        swordDown.physicsBody?.categoryBitMask = swordCategory
        swordDown.physicsBody?.contactTestBitMask = avatarCategory
        swordPair.addChild(swordDown)
        
        let swordUp = SKSpriteNode(texture: swordTextureUp)
        swordUp.setScale(2.0)
        swordUp.position = CGPoint(x: 0.0, y: y)
        swordUp.physicsBody = SKPhysicsBody(rectangleOfSize: swordUp.size)
        swordUp.physicsBody?.dynamic = false
        swordUp.physicsBody?.categoryBitMask = swordCategory
        swordUp.physicsBody?.contactTestBitMask = avatarCategory
        swordPair.addChild(swordUp)
        
        let contactNode = SKNode()
        contactNode.position = CGPoint(x: swordDown.size.width + avatar.size.width / 2, y: self.frame.midY)
        contactNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: swordUp.size.width, height: self.frame.size.height))
        contactNode.physicsBody?.dynamic = false
        contactNode.physicsBody?.categoryBitMask = scoreCategory
        contactNode.physicsBody?.contactTestBitMask = avatarCategory
        swordPair.addChild(contactNode)
        swordPair.runAction(moveSwordsAndRemove)
        swords.addChild(swordPair)
        
    }
    
    func resetScene (){
        // Move avatar to original position and reset velocity
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: -4.0)
        avatar.position = CGPoint(x: self.frame.size.width / 2.5, y: self.frame.midY)
        avatar.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        avatar.physicsBody?.collisionBitMask = worldCategory | swordCategory
        avatar.speed = 1.0
        avatar.zRotation = 0.0
        
        // Remove all existing swords
        swords.removeAllChildren()
        canRestart = false
        
        // Reset score
        score = 0
        scoreLabelNode.text = String(score)
        
        // Restart animation
        moving.speed = 1
        Singleton.sharedInstance.playing = true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Called when a touch begins
        if moving.speed > 0 {
            for touch: AnyObject in touches {
                _ = touch.locationInNode(self)
                avatar.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
                avatar.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 30))
            }
        } else if canRestart {
            self.resetScene()
        }
    }
    
    func clamp(min: CGFloat, max: CGFloat, value: CGFloat) -> CGFloat {
        if(value > max) {
            return max
        } else if(value < min) {
            return min
        } else {
            return value
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        // Called before each frame is rendered
        avatar.zRotation = self.clamp(-1, max: 0.5, value: avatar.physicsBody!.velocity.dy * (avatar.physicsBody!.velocity.dy < 0 ? 0.003 : 0.001))
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        if moving.speed > 0 {
            if (contact.bodyA.categoryBitMask & scoreCategory) == scoreCategory || (contact.bodyB.categoryBitMask & scoreCategory) == scoreCategory {
                // avatar has contact with score entity
                score += 1
                scoreLabelNode.text = String(score)

            } else {
                moving.speed = 0
                avatar.physicsBody?.collisionBitMask = worldCategory
                avatar.runAction(SKAction.rotateByAngle(CGFloat(M_PI) * CGFloat(avatar.position.y) * 0.01, duration:1), completion:{self.avatar.speed = 0})
                
                Singleton.sharedInstance.score = score
                Singleton.sharedInstance.gameOver = true
            }
        }
    }
}
