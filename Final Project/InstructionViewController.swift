//
//  InstructionViewController.swift
//  Final Project
//
//  Created by kuntzba1 on 4/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit
import Parse
import Bolts

class InstructionViewController: UIViewController{
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dirpath : NSString = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)[0]
        let scorePath = dirpath.stringByAppendingPathComponent("\(Singleton.sharedInstance.username).json")
        Singleton.sharedInstance.scorePath = scorePath
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func unwindToInstructions (segue: UIStoryboardSegue){
        let gameplayer = segue.sourceViewController as! PlayGameViewController
        gameplayer.backgroundMusicPlayer.stop()
    }
    
    @IBAction func performLogout(){
        PFUser.logOut()
        performSegueWithIdentifier("unwindToLogin", sender: self)
    }
}