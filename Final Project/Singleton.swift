//
//  Singleton.swift
//  Final Project
//
//  Created by kuntzba1 on 4/14/16.
//  Copyright © 2016 iOS. All rights reserved.
//

import UIKit

var contextVar = 0

// Singleton class for storing app-global information
class Singleton: NSObject  {
    static let sharedInstance : Singleton = {
        let instance = Singleton()
        return instance
    }()
    dynamic var scorePath = String()
    dynamic var score = 0
    dynamic var gameOver = false
    dynamic var playing = false
    dynamic var username = String()
    
    // Set global attributes for use in navigation bar formatting
    let attributes = [NSFontAttributeName : UIFont(name: "Copperplate", size: 35.0)!]
}
