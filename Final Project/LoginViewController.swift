//
//  LoginViewController.swift
//  Auto-Mobile Dev
//
//  Created by kuntzba1 on 3/16/16.
//  Copyright © 2016 Auto-Mobile. All rights reserved.
//

import UIKit
import Parse
import Bolts


class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var didShift = false
    let target = Singleton.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set text formatting for nav bar
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(Singleton.sharedInstance.attributes, forState: UIControlState.Normal)
        self.navigationController?.navigationBar.titleTextAttributes = Singleton.sharedInstance.attributes
    }
    
    @IBAction func dismissKeyboard(sender: UIControl) {
        // Dismisses keyboard
        self.view.endEditing(true)
    }
    
    @IBAction func loginButtonPress(sender: AnyObject) {
        target.username = self.usernameField.text!
        let password = self.passwordField.text
        
        // Validate the text fields
        if target.username.characters.count < 8 {
            // Display alertController for username character count
            let alert = UIAlertController(title: "Invalid", message: "Username must be at least 8 characters", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else if password?.characters.count < 8 {
            // Display alertController for password character count
            let alert = UIAlertController(title: "Invalid", message: "Password must be at least 8 characters", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                (action) in
            }
            alert.addAction(okAction)
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            // Run a spinner to show a task in progress
            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0, 0, 150, 150)) as UIActivityIndicatorView
            spinner.startAnimating()
            
            // Send a request to login
            PFUser.logInWithUsernameInBackground(target.username, password: password!, block: { (user, error) -> Void in
                
                // Stop the spinner
                spinner.stopAnimating()
                
                if ((user) != nil) {
                    self.performSuccessfulLogin()
                    
                } else {
                    // Display alertController for failure to login
                    let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
                        (action) in
                    }
                    alert.addAction(okAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func unwindToLogin (segue: UIStoryboardSegue){
        
    }
    
    @IBAction func doneRegistration (segue: UIStoryboardSegue){
        
    }
    
    func performSuccessfulLogin(){
        performSegueWithIdentifier("successfulLogin", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "successfulLogin" {
            // Clear fields
            usernameField.text = ""
            passwordField.text = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}